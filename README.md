Varnish VCL
-----------

```
if (beresp.status >= 200 && beresp.status < 400 && (req.request == "PUT" || req.request == "POST" || req.request == "DELETE")) {
  ban("obj.http.X-Invalidated-By ~ " + beresp.http.X-Invalidates);
}
```
